"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
var readline = __importStar(require("readline"));
var App = /** @class */ (function () {
    function App() {
        this.params = {
            columns: 6,
            roundsMax: 12,
            colors: 6
        };
        this.colors = {
            1: 'red',
            2: 'blue',
            3: 'white',
            4: 'green',
            5: 'black',
            6: 'yellow'
        };
        this.states = {
            playerProp: new Array(),
            winColor: 0,
            round: 1,
        };
        this.solution = { 1: [], 2: [], 3: [], 4: [], 5: [], 6: [] };
    }
    App.prototype.setSolution = function () {
        // for (let i = 0; i < this.params['columns']; i++) {
        //   let color = Math.floor(Math.random() * (this.params['colors']) + 1);
        //   this.solution[color].push(i);
        // }
        this.solution = {
            '1': [0, 1],
            '2': [],
            '3': [2, 3],
            '4': [],
            '5': [4],
            '6': [5]
        };
    };
    App.prototype.verifyProposition = function () {
        console.log(this.solution);
        console.log(this.states['playerProp']);
        this.states['winColor'] = 0;
        for (var i = 1; i <= this.params['columns']; i++) {
            var checkedColor = this.states['playerProp'][i - 1];
            if (this.solution[checkedColor].includes(i - 1)) {
                this.states['winColor'] += 1;
                console.log("\u001B[32m".concat(this.colors[checkedColor], " is at the right position"));
            }
            else if (this.solution[checkedColor].length != 0) {
                var count = 0;
                for (var j = 1; j <= this.params['columns']; j++) {
                    if (this.solution[j].includes(checkedColor))
                        count++;
                }
                if (count == this.solution[checkedColor].length) {
                    console.log("\u001B[33m".concat(this.colors[checkedColor], " has already been placed"));
                }
                else {
                    console.log("\u001B[34m".concat(this.colors[checkedColor], " is in the combination"));
                }
            }
            else {
                console.log("\u001B[31m".concat(this.colors[checkedColor], " isn't in the combination"));
            }
        }
    };
    App.prototype.checkWin = function () {
        return (this.states['playerProp'].length == this.params['columns'] &&
            this.states['winColor'] == this.params['columns']);
    };
    // async getInput() {
    //   this.states['playerProp'] = []
    //   const rl = readline.createInterface({
    //     input: process.stdin,
    //     output: process.stdout
    //   });
    //   let start = async () => {
    //     console.log('\x1b[37m', `[ROUND ${this.states['round']}] Choose a color: (red, blue, white, green, black, yellow): `);
    //     for await (const line of rl) {
    //       console.log('\x1b[37m', `[ROUND ${this.states['round']}] Choose a color: (red, blue, white, green, black, yellow): `);
    //       if (line == 'exit') {
    //         rl.close();
    //         process.exit(0);
    //       }
    //       let colorKey = Object.values(this.colors).indexOf(line);
    //       if (colorKey > -1) {
    //         this.states['playerProp'].push(colorKey + 1);
    //       }
    //       if (this.states['playerProp'].length >= this.params['columns']) {
    //         return;
    //       }
    //     }
    //   }
    //   await start();
    // }
    // async run() {    
    //   this.setSolution();
    //   while(this.states['round'] <= this.params['roundsMax']) {
    //     await this.getInput();
    //     this.verifyProposition();
    //     let winGame = this.checkWin();
    //     if (!winGame && this.states['round'] == this.params['roundsMax']) {
    //       console.log('Lose')
    //     } else if (winGame) {
    //       console.log('Win');
    //       process.exit(0);
    //     }
    //     this.states['round'] += 1
    //   }
    // }
    App.prototype.getInputPromise = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.states['playerProp'] = [];
            var ask = function () {
                var rlTest = readline.createInterface({
                    input: process.stdin,
                    output: process.stdout
                });
                return new Promise(function (resolve) {
                    rlTest.question("\u001B[37m[ROUND ".concat(_this.states['round'], "] Choose a color: (red, blue, white, green, black, yellow): "), function (answer) {
                        rlTest.close();
                        resolve(answer);
                    });
                });
            };
            var loopAsk = function () {
                return ask().then(function (answer) {
                    if (answer == 'exit') {
                        process.exit(0);
                    }
                    var colorKey = Object.values(_this.colors).indexOf(answer);
                    if (colorKey > -1) {
                        _this.states['playerProp'].push(colorKey + 1);
                    }
                    if (_this.states['playerProp'].length < _this.params['columns']) {
                        loopAsk();
                    }
                    else {
                        _this.states['round'] += 1;
                        resolve();
                    }
                });
            };
            loopAsk();
        });
    };
    App.prototype.runWithPromise = function () {
        var _this = this;
        if (this.states['round'] == 1) {
            this.setSolution();
        }
        this.getInputPromise().then(function () {
            _this.verifyProposition();
            var winGame = _this.checkWin();
            if (!winGame && _this.states['round'] == _this.params['roundsMax']) {
                console.log('Lose');
                return;
            }
            else if (winGame) {
                console.log('Win');
                return;
            }
            else {
                _this.runWithPromise();
            }
        });
    };
    return App;
}());
exports.App = App;
