import { App } from './app'

const app = new App();

app.runWithPromise();
// app.run();