import * as readline from 'readline';
export class App {

  params = {
    columns: 6,
    roundsMax: 12,
    colors: 6
  }

  colors: { [key: number]: string }  = {
    1: 'red',
    2: 'blue',
    3: 'white',
    4: 'green',
    5: 'black',
    6: 'yellow'
  }

  states = {
    playerProp: new Array(),
    winColor: 0,
    round: 1,
  }

  solution: { [key: number]: Array<number> } = { 1: [], 2: [], 3: [], 4: [], 5: [], 6: []};

  setSolution() {  
    for (let i = 0; i < this.params['columns']; i++) {
      let color = Math.floor(Math.random() * (this.params['colors']) + 1);
      this.solution[color].push(i);
    }
  }
  verifyProposition() {
    this.states['winColor'] = 0;

    for (let i = 1; i <= this.params['columns']; i++) {
      let checkedColor = this.states['playerProp'][i - 1];

      if (this.solution[checkedColor].includes(i - 1)) {
        this.states['winColor'] += 1;
        console.log(`\x1b[32m${this.colors[checkedColor]} is at the right position`);
      } else if (this.solution[checkedColor].length != 0) {
        let count = 0;

        for (let j = 1; j <= this.params['columns']; j++) {
          if (this.solution[j].includes(checkedColor))
            count++;
        }

        if (count == this.solution[checkedColor].length) {
          console.log(`\x1b[33m${this.colors[checkedColor]} has already been placed`);
        } else {
          console.log(`\x1b[34m${this.colors[checkedColor]} is in the combination`);
        }
      } else {
        console.log(`\x1b[31m${this.colors[checkedColor]} isn't in the combination`);
      }
    }
  }

  checkWin(): boolean {
    return (this.states['playerProp'].length == this.params['columns'] &&
            this.states['winColor'] == this.params['columns'])
  }

  // async getInput() {
  //   this.states['playerProp'] = []

  //   const rl = readline.createInterface({
  //     input: process.stdin,
  //     output: process.stdout
  //   });

  //   let start = async () => {
  //     console.log('\x1b[37m', `[ROUND ${this.states['round']}] Choose a color: (red, blue, white, green, black, yellow): `);
  //     for await (const line of rl) {
  //       console.log('\x1b[37m', `[ROUND ${this.states['round']}] Choose a color: (red, blue, white, green, black, yellow): `);

  //       if (line == 'exit') {
  //         rl.close();
  //         process.exit(0);
  //       }

  //       let colorKey = Object.values(this.colors).indexOf(line);
  //       if (colorKey > -1) {
  //         this.states['playerProp'].push(colorKey + 1);
  //       }
  //       if (this.states['playerProp'].length >= this.params['columns']) {
  //         return;
  //       }
  //     }
  //   }
  //   await start();
  // }

  // async run() {    
  //   this.setSolution();

  //   while(this.states['round'] <= this.params['roundsMax']) {
  //     await this.getInput();
  //     this.verifyProposition();
  //     let winGame = this.checkWin();

  //     if (!winGame && this.states['round'] == this.params['roundsMax']) {
  //       console.log('Lose')
  //     } else if (winGame) {
  //       console.log('Win');
  //       process.exit(0);
  //     }

  //     this.states['round'] += 1
  //   }
  // }

  getInputPromise() {
    return new Promise<void>(resolve => {
      this.states['playerProp'] = []

      const ask = () => {
        const rlTest = readline.createInterface({
          input: process.stdin,
          output: process.stdout
        });
  
        return new Promise<string>(resolve => {
          rlTest.question(`\x1b[37m[ROUND ${this.states['round']}] Choose a color: (red, blue, white, green, black, yellow): `, answer => {
            rlTest.close();
            resolve(answer);
          })
        })
      }
  
      const loopAsk = () => {
        return ask().then((answer) => {
  
          if (answer == 'exit') {
            process.exit(0);
          }
  
          let colorKey = Object.values(this.colors).indexOf(answer);
          if (colorKey > -1) {
            this.states['playerProp'].push(colorKey + 1);
          }
  
          if (this.states['playerProp'].length < this.params['columns']) {
            loopAsk();
          } else {
            this.states['round'] += 1;
            resolve()
          }
        })
      }
  
      loopAsk();
    })
  }

  runWithPromise() {
    if (this.states['round'] == 1) {
      this.setSolution();
    }

    this.getInputPromise().then(() => {
      this.verifyProposition();
      let winGame = this.checkWin();

      if (!winGame && this.states['round'] == this.params['roundsMax']) {
        console.log('Lose');
        return;
      } else if (winGame) {
        console.log('Win');
        return;
      } else {
        this.runWithPromise();
      }
    });
  }
}